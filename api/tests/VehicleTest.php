<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class VehicleTest extends TestCase
{
    private $sample_name = 'Hello World';
    private $sample_engine_power = '300kw';
    private $sample_price = '$400';
    private $sample_location = 'Philippines';

    /**
     * Test POST /vehicles route with engine displacement in liters
     *
     * @return void
     */
    public function testCreateVehicleWithEngineDisplacementInLiters()
    {
        $response = $this->call('POST', '/api/v1/vehicles', [
            'name' => $this->sample_name,
            'engineDispValue' => 1.33,
            'engineDispUnit' => 'liters',
            'enginePower' => $this->sample_engine_power,
            'price' => $this->sample_price,
            'location' => $this->sample_location
        ]);

        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'name',
            'engine_power',
            'price',
            'location',
            'engine_displacement'
        ]);
        $this->assertEquals(
            '1.33 liters / 1330.00 cc / 81.16 cu in',
            $response->getData()->engine_displacement
        );
    }

    /**
     * Test POST /vehicles route with engine displacement in cc
     *
     * @return void
     */
    public function testCreateVehicleWithEngineDisplacementInCubicCentimeters()
    {
        $response = $this->call('POST', '/api/v1/vehicles', [
            'name' => $this->sample_name,
            'engineDispValue' => 217.29,
            'engineDispUnit' => 'cubic centimeters',
            'enginePower' => $this->sample_engine_power,
            'price' => $this->sample_price,
            'location' => $this->sample_location
        ]);

        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'name',
            'engine_power',
            'price',
            'location',
            'engine_displacement'
        ]);
        $this->assertEquals(
            '0.22 liters / 217.29 cc / 13.26 cu in',
            $response->getData()->engine_displacement
        );
    }

    /**
     * Test POST /vehicles route with engine displacement in cubic inches
     *
     * @return void
     */
    public function testCreateVehicleWithEngineDisplacementInCubicInches()
    {
        $response = $this->call('POST', '/api/v1/vehicles', [
            'name' => $this->sample_name,
            'engineDispValue' => 23.33,
            'engineDispUnit' => 'cubic inches',
            'enginePower' => $this->sample_engine_power,
            'price' => $this->sample_price,
            'location' => $this->sample_location
        ]);

        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'name',
            'engine_power',
            'price',
            'location',
            'engine_displacement'
        ]);
        $this->assertEquals(
            '0.38 liters / 382.31 cc / 23.33 cu in',
            $response->getData()->engine_displacement
        );
    }

    /**
     * Test GET /vehicles route
     *
     * @return void
     */
    public function testVehicleList() {
        $this->call('GET', '/api/v1/vehicles');
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            '*' => [
                'id',
                'name',
                'engine_power',
                'price',
                'location',
                'engine_displacement'
            ]
        ]);
    }
}
