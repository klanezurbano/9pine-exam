<?php

namespace App\Models;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

use PhpUnitsOfMeasure\PhysicalQuantity\Volume;

class Vehicle extends Model
{
    use UsesUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'engine_disp_liters',
        'engine_power',
        'price',
        'location'
    ];

    protected $appends = ['engine_displacement'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'engine_disp_liters',
        'created_at',
        'updated_at'
    ];

    public function getEngineDisplacementAttribute() {
        $value_in_liters = $this->engine_disp_liters;

        $rounded_liters = round($value_in_liters, 2);

        $value_in_cc = (new Volume($value_in_liters, 'liters'))->toUnit('cubic centimeters');
        $rounded_cc = round($value_in_cc, 2);

        $value_in_cuin = (new Volume($value_in_liters, 'liters'))->toUnit('cubic inches');
        $rounded_cuin = round($value_in_cuin, 2);

        return sprintf('%.2f liters / %.2f cc / %.2f cu in', $rounded_liters, $rounded_cc, $rounded_cuin);
    }
}