<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Http\Request;

use PhpUnitsOfMeasure\PhysicalQuantity\Volume;

class VehicleController extends Controller
{

    public function index() {
        $vehicles = Vehicle::all();

        return response()->json($vehicles, 200);
    }

    public function create(Request $request)
    {
        $vehicle = new Vehicle;
        $vehicle->name = $request->name;
        
        // always save in liters
        $engine_displacement = new Volume($request->engineDispValue, $request->engineDispUnit);
        $vehicle->engine_disp_liters = $engine_displacement->toUnit('liters');
        
        $vehicle->engine_power = $request->enginePower;
        $vehicle->price = $request->price;
        $vehicle->location = $request->location;

        $vehicle->save();

        return response()->json($vehicle, 201);
    }
}