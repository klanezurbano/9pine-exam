import VehicleList from "@/components/VehicleList/index.vue";
import VehicleForm from "@/components/VehicleForm/index.vue";

export default {
  name: "VehiclePage",
  components: {
    VehicleList,
    VehicleForm
  }
};
