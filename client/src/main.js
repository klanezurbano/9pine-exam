import ElementPlus from "element-plus";
import "element-plus/lib/theme-chalk/index.css";

import axios from "axios";
import VueAxios from "vue-axios";

import { createApp } from "vue";
import VehiclePage from "./views/VehiclePage/index.vue";

createApp(VehiclePage)
  .use(ElementPlus)
  .use(VueAxios, axios)
  .mount("#app");
