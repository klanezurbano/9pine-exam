export default {
  name: "VehicleForm",
  data: () => ({
    formData: {},
    formRules: {
      name: [
        {
          required: true,
          message: "Please input vehicle name",
          trigger: "blur"
        },
        {
          min: 5,
          message: "Length should be more than 5 characters",
          trigger: "blur"
        }
      ],
      engineDispValue: [
        { required: true, message: "required", trigger: "blur" }
      ],
      enginePower: [
        {
          required: true,
          message: "Please input engine power value",
          trigger: "blur"
        }
      ],
      price: [
        { required: true, message: "Please input price", trigger: "blur" }
      ],
      location: [
        { required: true, message: "Please input location", trigger: "blur" }
      ]
    }
  }),
  created() {
    this.clear();
  },
  methods: {
    async onSubmit() {
      let allValid = true;
      this.$refs["formData"].validate(valid => {
        if (!valid) {
          allValid = false;
        }

        return valid;
      });

      if (!allValid) return;

      const response = await this.axios.post(
        `${process.env.VUE_APP_API_URL}/vehicles`,
        this.formData
      );

      console.log(response.status);
      location.reload();
    },
    clear() {
      this.formData = {
        name: "",
        engineDispValue: 0,
        engineDispUnit: "liters",
        enginePower: "",
        price: "",
        location: ""
      };
    }
  }
};
