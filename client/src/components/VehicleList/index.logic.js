export default {
  name: "VehicleList",
  data: () => ({
    vehicles: []
  }),
  async created() {
    const response = await this.axios.get(
      `${process.env.VUE_APP_API_URL}/vehicles`
    );

    this.vehicles = response.data;
    console.log(response.data);
  }
};
